class Operation {
	protected:
		char* operation;
	public 
		Operation(char* operation);
	
		char* getType();
		bool setType(*char operation);
		char* runAction(char* type);
}